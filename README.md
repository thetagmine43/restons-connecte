
# Explication rapide

Création d'une API pour gérer un pays, le pays contient un nom, un code et un id unique.
Le code à uniquement 2 caractères. Différentes routes ont été crée pour effectuer différentes actions.

Sur la partie front on peut voir un tableau restituant les différents pays ajoutés ainsi qu'un bouton pour les supprimer et un boutton pour les modifiers.
Il y a aussi 2 input qui servent pour l'ajout d'un pays.

# Pré requis installation

Le serveur Mysql utilisant le port 3308.
L'utilisateur est root sans mdp. On peut aller changer ces informations de connection dans le fichier application.properties.

# Installation du projet

Pour lancer le projet il suffit de faire la commande `./mvnw spring-boot:run` (sous Mac/Linux)
ou `mvnw spring-boot:run` (sous Windows). La commande est à faire sur la racine du projet.

Pour lancer le fichier jar du projet il est possible de faire la commande : `java -jar restons-0.0.1.jar`.

Une fois le projet lancé, il est lancé sur le serveur localhost:8080. Il est possible de se rendre sur http://localhost:8080/index

Les actions réalisés avec l'index seront afficher dans la console du navigateur.

Ajouter un pays avec l'index ou avec la commande curl.

# Commande utile

Commande curl :

GET /countries : `curl --location --request GET 'localhost:8080/restons/countries'`

GET /country/{id} : `curl --location --request GET 'localhost:8080/restons/country/1'`

POST /country : `curl --location --request POST 'localhost:8080/restons/add' \
--header 'Content-Type: application/json' \
--data-raw '{
"name" : "America",
"code" : "US"
}'`

PUT /country/{id} : `curl --location --request PUT 'localhost:8080/restons/country/1' \
--header 'Content-Type: application/json' \
--data-raw '{
"name" : "France",
"code" : "US"
}'`

DELETE /country/{id} : `curl --location --request DELETE 'localhost:8080/restons/country/1'`

Commande Maven : 

Generer le jar : `./mvnw clean install`
