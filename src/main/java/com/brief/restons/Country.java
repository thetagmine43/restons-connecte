package com.brief.restons;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity // This tells Hibernate to make a table out of this class
public class Country {
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Integer id;

  @NotNull
  @Size(min = 1, message = "Le nom doit être renseigné")
  @Pattern(regexp="^[A-Za-z]*$",message = "Le nom doit uniquement avoir des lettres")
  private String name;

  @NotNull
  @Size(min = 2, max = 2, message = "Le code doit contenir 2 caractères")
  @Pattern(regexp="^[A-Za-z]*$",message = "Le code doit contenir 2 caractères")
  private String code;

  public Country(String name, String code) {
    this.name = name;
    this.code = code;
  }

  public Country(){

  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code.toUpperCase();
  }
}