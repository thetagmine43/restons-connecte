async function show() {

    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    };

    fetch("/restons/countries", requestOptions)
        .then(response => response.json())
        .then(result => displayCountry(result))
        .catch(error => console.log('error', error));

}

function displayCountry(result) {

    result.forEach(element => {
        console.log(element);
        let ligne = document.createElement("tr");
        ligne.innerHTML += "<td>" + element.name + "</td>";
        ligne.innerHTML += "<td>" + element.code + "</td>";
        ligne.innerHTML += "<td> <button type=\"button\" onclick=\"deleteCountry("+ element.id +")\">delete</button> </td>";
        ligne.innerHTML += "<td> <button type=\"button\" onclick=\"updateInput("+ element.id + "," + "'"+ element.name +"'" + "," + "'"+ element.code +"'" +")\">update</button> </td>";
        document.querySelector("#tableCountry").appendChild(ligne);
    });
}

this.show();

const el = document.getElementById("submit");
el.addEventListener("click", addCountry);

function addCountry() {
    let name = document.getElementById("name");
    let code = document.getElementById("code");

    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    let raw = JSON.stringify({
        "name": name.value,
        "code": code.value
    });

    let requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };

    fetch("/restons/add", requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));
}

function deleteCountry(id) {
    let requestOptions = {
        method: 'DELETE',
        redirect: 'follow'
    };

    fetch("/restons/country/"+ id, requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));
}

function updateInput(id, name, code) {
    let nameEl = document.getElementById("name");
    let codeEl = document.getElementById("code");
    let buttonSubmit = document.getElementById("submit");
    let buttonUpdate = document.getElementById("update");

    nameEl.value = name;
    codeEl.value = code;

    buttonSubmit.style.display = 'none';
    buttonUpdate.style.display = 'inline-block';

    buttonUpdate.onclick = function () {
        updateCountry(id);
    };

}

function updateCountry(id){
    let nameEl = document.getElementById("name");
    let codeEl = document.getElementById("code");

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
        "name": nameEl.value,
        "code": codeEl.value
    });

    var requestOptions = {
        method: 'PUT',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };

    fetch("/restons/country/" + id, requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));

}